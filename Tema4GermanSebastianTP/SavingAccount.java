

import java.io.Serializable;

public class SavingAccount extends Account implements Serializable
{ 	private static int dobanda;
    private int suma;
    static Person pers;
    static double sum;
    static String name;
	public SavingAccount(int dobanda, int suma)
	{
        super( suma, pers, sum, name);
		this.dobanda=dobanda;
		this.suma=suma;
	}
	public SavingAccount(){
		super(dobanda, pers, sum, name);
	}

	
	public int calculeazaDobanda(int a)
	{
		return 15*a/85;
	}

	public void adaugaSuma(int a)
	{
		this.suma=(int) (0.7*this.suma+a);
	}

    public void retrage(int a){
    	this.suma-=a;
    }
}

