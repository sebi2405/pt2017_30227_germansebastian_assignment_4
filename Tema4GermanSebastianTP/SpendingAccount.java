

import java.io.Serializable;

public class SpendingAccount extends Account implements Serializable{	
	
	private static String tip="Spending";	
	private int suma;
	public SpendingAccount(int ID, Person person,double balance)
	{
		super(ID,person,balance, tip);
	
	}

	 public int adaugaSuma(int a)
	   {
	   return this.suma=this.suma+a;
	   } 
	   public int retrageSuma(int a)
	   {
	     return this.suma=this.suma-a;  
	   }
}