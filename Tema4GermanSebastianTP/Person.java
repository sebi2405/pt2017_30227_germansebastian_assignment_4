

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;

public class Person implements Serializable, Observer {

	private String Name;
	private String Surname;
	private String adr;
	private HashSet<Account> conturi;

	public Person(String Name, String Surname,String adr) {
		this.Name = Name;
		this.Surname = Surname;
		this.adr=adr;
	}

	public boolean equals(Person pers) {
		if (this == pers)
			return true;
		else return false;
		
	}

	public String getName() {
		return this.Name;
	}

	public HashSet<Account> getAccounts() {
		return conturi;
	}

	public void addAccount(Account account) {
		this.conturi.add(account);
	}

	public void removeAccount(Account account) {
		this.conturi.remove(account);
	}

	public void setAccounts(HashSet<Account> accounts) {
		this.conturi = accounts;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getSurname() {
		return Surname;
	}

	public void setSurname(String surname) {
		Surname = surname;
	}


	public void update(Observable arg0, Object arg1) {
		
	}

	 public boolean wellFormed()
     { 
     if (Name.isEmpty()==true)
     return false;
     if(Surname.length()!=13)
         return false;
     if(adr.isEmpty()==true)
         return false;
     return true;
     }    

}
