

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;


public class Bank implements BankProc, Serializable {
	public HashMap<Person, HashSet<Account>> bank;
	private Hashtable<Integer,ArrayList<Account>> hash = new Hashtable<Integer,ArrayList<Account>> ();
	private ArrayList<Account> a = new ArrayList<Account>();   
	
	public Bank() {
		bank = new HashMap<Person, HashSet<Account>>();
		hash=new Hashtable<Integer,ArrayList<Account>>();
		a=new ArrayList<Account>();
		
	}

	
	public void setBank(HashMap<Person, HashSet<Account>> bank) {
		this.bank = bank;
	}


	public void addPerson(Person p) {
		assert isWellFormed();
		assert p!=null;
		bank.put(p, p.getAccounts());
	}

	 public void removeAccounts(int id){       
	       try{
	       assert (hash.size()>0); 
	       assert ( id>0); 
	       for( int i=0;i<hash.size();i++)
	       {
	       if (id==i){
	        hash.remove(id);
	        System.out.println( hash.remove(i));
	       } 
	       }
	       }
	        catch(AssertionError e){e.printStackTrace(); }   
	   }

	
	public void addAccount(Person person, Account account) {
		assert isWellFormed();
		assert person !=null : "Persoana = NULL";
		assert account != null: "Valoare cont=NULL";
		person.addAccount(account);
		bank.put(person, person.getAccounts());
	}

	
	public void removeAccount(Person person, Account account) {
		assert isWellFormed();
		assert person !=null: "Persoana = NULL";
		assert account !=null : "Valoare cont=NULL";
		HashSet<Account> set = bank.get(person);
		Iterator<Account> it = set.iterator();
		Account temp;
		while (it.hasNext()) {
			temp = it.next();
			if (temp.equals(account)) {
				it.remove();
			}
		}
	}

	
	public Account readAccount(Person person, int id) {
		assert isWellFormed();
		assert person!=null: "Person==NULL";
		assert id!=0:"ID==NULL";
		if (this.bank.containsKey(person)) {
			HashSet<Account> set = bank.get(person);
			for (Account temp : set) {
				
					return temp;
			}
		}
		return null;
	}

	@Override
	public void writeAccount(Person person, Account account, int id) {
		assert isWellFormed();
		assert person!=null: "Person==NULL";
		assert account!=null: "Account==NULL";
		assert id!=0: "id==0";
		if (this.bank.containsKey(person)) {
			HashSet<Account> set = bank.get(person);
			for (Account temp : set) {
				
				set.remove(temp);
				set.add(account);
			}
		}
	}
	 public int searchAccount(int id){
         try{
        	 assert (!hash.isEmpty()); 
        	 assert ( id>0); 
            boolean gasit=false;
            for( int i=0;i<hash.size();i++){
                System.out.println(hash.get(i));
               return i;              
            }           
    }
    catch(AssertionError e){e.printStackTrace();}
         return id;
     
}
	public boolean isWellFormed()
	{
		if(bank==null)
			return false;
		if(hash==null)
			return false;
		if(a==null)
			return false;
		return true;
	}



	public void removePerson(Person P) {
		
		
	}


	@Override
	public void addAccounts(String nume, String prenume, String CNP, String adresa, int id) {
		// TODO Auto-generated method stub
		
	}

}
