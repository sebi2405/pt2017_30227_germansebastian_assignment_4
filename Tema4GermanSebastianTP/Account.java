

import java.io.Serializable;

public class Account implements Serializable
{
	public int ID;
	private Person pers;
	private double sum;
    private String name;
    public Account(int ID, Person person, double balance,String name)
    {
        this.ID = ID;
        this.pers = person;
        this.sum = balance;
        this.name=name;
    }
  
	

	
    public Boolean wellFormed()
    {
        if(pers.wellFormed()==false)
            return false;
        if(sum<0)
            return false;
        if(name.length()<0)
            return false;
        return true;
            
    }


	public void deposit(double amount)
	{
		this.sum += amount;
	}
	
	public double withdrawal(double amount)
    {
        if(amount <= this.sum)
        {
            this.sum -= amount;
            return amount;
        }
        else
        	sum=sum-(-amount);
        return amount;
    }	
	public void setBalance(double balance) {
		this.sum = balance;
	}
	
	public void setPerson(Person person) {
		pers = person;
	}
	
	public void setId(int id) {
		this.ID = id;
	}
	public void setName(String name){
		this.name=name;
	}
}
